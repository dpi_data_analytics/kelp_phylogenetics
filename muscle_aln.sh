#!/bin/bash

# aln sequences
for gene in protein_seqs/*
do
	echo $gene
	out=protein_aln/$(basename $gene)
	muscle -in $gene -out $out
done